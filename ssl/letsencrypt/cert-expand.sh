sudo certbot certonly \
  --agree-tos \
  --email $1 \
  --manual \
  --preferred-challenges=dns \
  -d $2 \
  -d *.$2 \
  --expand \
  --server https://acme-v02.api.letsencrypt.org/directory